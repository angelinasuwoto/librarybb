import java.util.ArrayList;
import java.util.Scanner;

public class Library {
    private static ArrayList<LibraryItem> catalog;
    private static ArrayList<LibraryItem> rentedOut;

    public Library() {
        catalog = new ArrayList<LibraryItem>();
        rentedOut = new ArrayList<LibraryItem>();
    }

    public static void addItem(LibraryItem item) {
        catalog.add(item);
        System.out.println(item.getName() + " added to library.");
    }

    public void removeItem(LibraryItem item) {
        for (int i = 0; i < catalog.size(); i++) {
            if ((catalog.get(i).getName()).equals(item.getName())) {
                catalog.remove(i);
            }
        }
        System.out.println(item.getName() + " removed from library.");
    }

    public void borrowItem(LibraryItem item) {
        if (!item.isBorrowable()) {
            System.out.println("You cannot borrow this item.");
        } else {
            removeItem(item);
            rentedOut.add(item);
            System.out.println(item.getName() + " borrowed from library.");
        }

    }

    public void returnItem(LibraryItem item) {
        for (int i = 0; i < rentedOut.size(); i++) {
            if ((rentedOut.get(i).getName()).equals(item.getName())) {
                rentedOut.remove(i);
            }
        }
        catalog.add(item);
        System.out.println(item.getName() + " returned to library.");
    }

    public void getCatalog() {
        System.out.println("Printing library catalog-------------------------");
        for (LibraryItem i : catalog) {
            System.out.println(i.getName());
        }
        System.out.println("-------------------------------------------------");
    }

    public void getRented() {
        System.out.println("Printing rented items-------------------------");
        for (LibraryItem i : rentedOut) {
            System.out.println(i.getName());
        }
        System.out.println("-------------------------------------------------");
    }

    public static void main(String[] args) {
        Library lib = new Library();
        Book harrypotter = new Book("Harry Potter");
        Book uwu = new Book("UWU for Beginners");
        Book hewwoworld = new Book("Hewwo World");
        CD lofi = new CD("Lofi Chill Hip Hop Beats");
        CD abcs = new CD("Let's Sing ABCs");
        CD blackpink = new CD("Blackpink EP. 1");
        Magazine famcircle = new Magazine("Famiwy Circle, August 2020");
        Magazine time = new Magazine("TIME Magazine, June 23 AD.");
        System.out.println("Adding items to library catalog...");
        lib.addItem(harrypotter);
        lib.addItem(uwu);
        lib.addItem(hewwoworld);
        lib.addItem(lofi);
        lib.addItem(abcs);
        lib.addItem(blackpink);
        lib.addItem(famcircle);
        lib.addItem(time);
        lib.getCatalog();

        Scanner input = new Scanner(System.in);
        System.out.println("Do you want to add the new book, 'I am Vewy Scawy' to the library? (Y/N)");
        char userInput = input.next().charAt(0);
        if (userInput == 'Y') {
            lib.addItem(new Book("I am Vewy Scawy"));
        }
        lib.getCatalog();

        System.out.println("Do you want to remove the CD, 'Let's Sing ABCs' from the library? (Y/N)");
        userInput = input.next().charAt(0);
        if (userInput == 'Y') {
            lib.removeItem(abcs);
        }
        lib.getCatalog();

        System.out.println("Do you want to borrow the book, 'Hewwo World' from the library? (Y/N)");
        userInput = input.next().charAt(0);
        if (userInput == 'Y') {
            lib.borrowItem(hewwoworld);
        }
        lib.getCatalog();
        lib.getRented();

        System.out.println("Do you want to borrow the magazine, 'TIME Magazine, June 23 AD.' from the library? (Y/N)");
        userInput = input.next().charAt(0);
        if (userInput == 'Y') {
            lib.borrowItem(time);
        }
        lib.getCatalog();
        lib.getRented();

        if (lib.rentedOut.contains(hewwoworld)) {
            System.out.println("Do you want to return the book, 'Hewwo World' to the library? (Y/N)");
            userInput = input.next().charAt(0);
            if (userInput == 'Y') {
                lib.returnItem(hewwoworld);
            }
            lib.getCatalog();
            lib.getRented();
        }

    }
}