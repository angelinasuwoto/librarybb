public class Magazine extends LibraryItem {

    public Magazine(String name) {
        super(name);
        canBorrow = false;
    }

}