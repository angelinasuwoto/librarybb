public abstract class LibraryItem {
    private String name;
    protected boolean canBorrow;

    public LibraryItem(String name) {
        this.name = name;
        this.canBorrow = true;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isBorrowable() {
        return canBorrow;
    }
}